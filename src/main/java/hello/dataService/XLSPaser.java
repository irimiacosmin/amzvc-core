package hello.dataService;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.monitorjbl.xlsx.StreamingReader;

public class XLSPaser {
    private static final String SAMPLE_XLSX_FILE_PATH = "C:\\Z_Things\\Repos\\amzvc-core\\data\\1 mai.xlsx";

    private static final Map<String, String> MONTHS =
            Collections.unmodifiableMap(new HashMap<String, String>() {
                {
                    put("1", "January");
                    put("2", "February");
                    put("3", "March");
                    put("4", "April");
                    put("5", "May");
                    put("6", "June");
                    put("7", "July");
                    put("8", "August");
                    put("9", "September");
                    put("10", "October");
                    put("11", "November");
                    put("12", "December");
                }
            });

    private List<Map<String, Object>> mapList = new ArrayList<>();

    public void start(){
        try {
            InputStream is = new FileInputStream(new File(SAMPLE_XLSX_FILE_PATH));
            Workbook workbook = StreamingReader.builder()
                    .rowCacheSize(1000)
                    .bufferSize(4096)
                    .open(is);

                for (Row row : workbook.getSheetAt(0)) {
                    Map<String, Object> map = new HashMap<>();
                    String[] dateArray = null;
                    if(row.getRowNum() == 0) {
                        String date = row.getCell(4).getStringCellValue();
                        date = date.split("=")[1].split(" - ")[0].substring(1);
                        dateArray = date.split("/");
                    }else if(row.getRowNum() > 1){
                        map.put("term", row.getCell(1));


                    }
                }

        }catch (Exception e){

            e.printStackTrace();
        }

    }
}
