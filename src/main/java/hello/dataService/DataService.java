package hello.dataService;

public class DataService {


    public static void main(String[] args) {
        //System.out.println(" > XLS Parser started");
        //XLSPaser xlsPaser = new XLSPaser();
        //xlsPaser.start();
        //System.out.println(" > XLS Parser ended work");
        System.out.println("SELECT count(*) "
                + "FROM order_generic.order_state os "
                + "JOIN order_generic.order_version ov ON os.order_version_id = ov.id "
                + "JOIN order_generic.order_header oh ON ov.order_id = oh.id "
                + "WHERE os.id = :orderStateId AND oh.agency_id = :agencyId");
    }
}
